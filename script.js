/* Відповідь 1:

Елемент input призначений для введення даних, але користувачі можуть використовувати його не лише за допомогою клавіатури.
Вони також можуть вводити дані мишею, трекпадом, голосом, рукописним вводом тощо.*/

// Задача:
const keys = document.querySelector(".btn-wrapper").children;

document.addEventListener("keydown", (e) => {
  [...keys].forEach((el) => {
    el.style.backgroundColor = "black";
    if (el.innerText === keyName(e)) {
      el.style.backgroundColor = "blue";
    }
  });
});

function keyName(event) {
  if (event instanceof Event) {
    if (event.code.startsWith("Key")) {
      return event.code.substring(3);
    } else if (event.code.startsWith("Digit")) {
      return event.code.substring(5);
    } else {
      return event.code;
    }
  } else {
    throw new Error(
      "The argument submitted to keyName() function is not an event!"
    );
  }
}
